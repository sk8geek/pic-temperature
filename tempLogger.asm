; Temperature Logger
    
; Version 0.3
; February 2021
;    
; Copyright (C) 2020-2021 Steven J Lilley
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
    processor 16F684

    #include <xc.inc>
    
; CONFIG
  CONFIG  FOSC = INTOSCIO       ; Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA4/OSC2/CLKOUT pin, I/O function on RA5/OSC1/CLKIN)
  CONFIG  WDTE = ON             ; Watchdog Timer Enable bit (WDT enabled)
  CONFIG  PWRTE = ON            ; Power-up Timer Enable bit (PWRT enabled)
  CONFIG  MCLRE = ON            ; MCLR Pin Function Select bit (MCLR pin function is MCLR)
  CONFIG  CP = OFF              ; Code Protection bit (Program memory code protection is disabled)
  CONFIG  CPD = OFF             ; Data Code Protection bit (Data memory code protection is disabled)
  CONFIG  BOREN = NSLEEP        ; Brown Out Detect (BOR enabled during operation and disabled in Sleep)
  CONFIG  IESO = OFF            ; Internal External Switchover bit (Internal External Switchover mode is disabled)
  CONFIG  FCMEN = OFF           ; Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is disabled)

; Registers

    APP_STATUS          equ 0x20    ; 
    PARITY              equ 0x21    ; bit 0 is used as a parity flag
    BIT_COUNT           equ 0x22    ; transmitted bit counter
    MINS_ALARM          equ 0x23    ; Minutes alarm value
    HOUR_ALARM          equ 0x24    ; Hours alarm value
    BYTE_COUNT          equ 0x25    ; number of data bytes to read/write from/to RTC
    ADC_HIGH            equ 0x26    ; Analog high byte
    ADC_LOW             equ 0x27    ; Analog low byte
    DISP_FAIL_COUNT     equ 0x28    ; display fail counter
      
                                    ;                   SPI     I2C
    SER_DATA0           equ 0x29    ; Serial data 0     -       Control
    SER_DATA1           equ 0x2a    ; Serial data 1     Addr    Addr
    SER_DATA2           equ 0x2b    ; Serial data 2
    SER_DATA3           equ 0x2c    ; Serial data 3
    SER_DATA4           equ 0x2d    ; Serial data 4
    SER_DATA5           equ 0x2e    ; Serial data 5
    SER_DATA6           equ 0x2f    ; Serial data 6
    SER_DATA7           equ 0x30    ; Serial data 7
    SER_DATA8           equ 0x31    ; Serial data 8

; PORTA bits
    I2C_DATA            equ 5       ; I2C data
    I2C_CLOCK           equ 4       ; I2C clock
    RTC_INT             equ 2       ; RTC interrupt input
        
; PORTC bits
    LUM_SENSOR          equ 0       ; luminance input
    TEMP_SENSOR         equ 1       ; temperature input
    RTC_ENABLE          equ 2       ; RTC enable output
    SPI_CLOCK           equ 3       ; SPI clock output
    MISO                equ 4       ; SPI master data input
    MOSI                equ 5       ; SPI master data output


; INTCON register
    GLOBAL_INT_EN       equ 7       ; Global interrupt enable flag bit
    EXT_INT             equ 1
     
; STATUS register
    REGBANK             equ 5       ; Bank indicator
    WATCHDOG            equ 4       ; Watchdog timeout
    ZEROFLAG            equ 2       ; Zero flag
    DIGITCARRY          equ 1       ; Digit carry flag
    CARRYBIT            equ 0       ; Carry flag
        
; **** Macros ****
      
disableInterrupts   MACRO
    bcf     INTCON, GLOBAL_INT_EN
    btfsc   INTCON, GLOBAL_INT_EN
    goto    $-2
ENDM

    
PSECT edata
    dw      0x70    ; control
    dw      0x01    ; register
    dw      0x0f    ; decode mode
    dw      0x18    ; intensity
    dw      0x03    ; scan limit
    dw      0x01    ; configuration
    dw      0x70    ; control
    dw      0x24    ; register
    dw      0x40    ; segment control
    ; starts at 0x09 
    dw      0x05    ; r
    dw      0x0f    ; t
    dw      0x0d    ; c
            
PSECT code abs
ORG 0x000
 
por:        
    call initialize

standby:
    sleep
    nop
    nop
    
interrupt:  
    disableInterrupts
    btfss   STATUS, WATCHDOG
    call    watchdogTimeOut
    btfsc   INTCON, EXT_INT
    goto    adcReadAfter
    btfsc   PIR1, 6
    call    recordTemp
    goto    standby
adcReadAfter:
    bsf     PORTA, 0            ; test LED on
    call    rtcInterrupt
    bsf     ADCON0, 0           ; enable ADC
    bsf     ADCON0, 1           ; start ADC conversion
    sleep
    nop
    bcf     PORTA, 0            ; test LED off
    goto    interrupt

watchdogTimeOut:
    btfsc   APP_STATUS, 7
    goto    showTemp
    call    readRtcTime
    call    displayTime
    bsf     APP_STATUS, 7
    retfie
    
showTemp:
    //call    displayTempRaw
    call    displayTemp
    bcf     APP_STATUS, 7
    retfie

; Handle RB0/INT (generated by INT0 on the RTC)
;
; Read RTC from registers 1 to 7
;
; This gives us minutes through to alarm 0 seconds. We must read 
; the alarm register to clear the interrupt.
;
rtcInterrupt:     
    disableInterrupts
    clrwdt
    call    readRtcTime
    call    displayTime
    bcf     INTCON, EXT_INT
    retfie
    
readRtcTime:
    movlw   0x07
    movwf   BYTE_COUNT
    movlw   SER_DATA1
    movwf   FSR
    movlw   0x01
    movwf   INDF
    call    rtcRead
    return

; Display the time
;
; This is called by the rtcInterrupt routine after it has read the time 
; into registers SER_DATA2 onward. We are only interested in minutes 
; and hours. These need spliting into tens and units for the display driver.
;
;   Register        After rtcRead       Required for display
;   ---------       ---------------     ---------------------
;   SER_DATA0       [unknown]           control (0x70)
;   SER_DATA1       address (0x01)      address (0x20)
;   SER_DATA2       minutes             minutes units
;   SER_DATA3       hours               minutes tens
;   SER_DATA4       day                 hours units
;   SER_DATA5       date                hours tens
;   SER_DATA6       month               [don't care]
;   SER_DATA7       year                [don't care]
;   SER_DATA8       alarm0 seconds      [don't care]
;            
displayTime:   
    clrwdt
    movlw   0x40
    movwf   SER_DATA6           ; display period between hours and minutes
    swapf   SER_DATA3, w        ; move hours tens to SER_DATA5
    andlw   0x0f
    movwf   SER_DATA5
    movf    SER_DATA3, w        ; move hours minutes to SER_DATA4
    andlw   0x0f
    movwf   SER_DATA4
    swapf   SER_DATA2, w        ; move minutes tens to SER_DATA3
    andlw   0x0f
    movwf   SER_DATA3
    movf    SER_DATA2, w        ; remove tens from minutes
    andlw   0x0f
    movwf   SER_DATA2
    movlw   SER_DATA1           ; point FSR to SER_DATA1
    movwf   FSR
    movlw   0x20                ; set address
    movwf   INDF
    decf    FSR, f
    movlw   0x70                ; set control
    movwf   INDF
    movlw   0x05                ; set data count
    movwf   BYTE_COUNT
    call    displayWrite
    //call    resetDisplayDecodeMode
    //return        

    ; NB: the order of these routines is important!
    ;
    ; In order for the time to be correctly displayed all digits MUST
    ; be decoded. We allow the displayTime routine to all through 
    ; to the following to routines.
    
    ; Reset the display decode mode so that ALL digits are decoded
resetDisplayDecodeMode:
    movlw   0x0f
    movwf   SER_DATA2           ; decode on for all segments

    ; Set display decode mode
    ; The value in SER_DATA2 must be set before calling this routine.
setDisplayDecodeMode:
    movlw   0x01
    movwf   SER_DATA1           ; Decode mode register
    movlw   SER_DATA0           ; point FSR to SER_DATA0
    movwf   FSR
    movlw   0x70                ; set control
    movwf   INDF
    movlw   0x01                ; set data count
    movwf   BYTE_COUNT
    call    displayWrite
    return
    
    ; Called after ADC complete interrupt
    ; Clears the interrupt, copies the value to general purpose registers
    ; and disabled the A/D converter.
recordTemp:
    disableInterrupts
    clrwdt
    movf    ADRESH, w
    movwf   ADC_HIGH
    bsf     STATUS, REGBANK     ; use bank 1
    movf    ADRESL, w
    bcf     STATUS, REGBANK     ; use bank 0
    movwf   ADC_LOW
    bcf     ADCON0, 0           ; disable ADC
    bcf     PIR1, 6             ; clear interrupt
    retfie
    
displayTempRaw:
    clrf    SER_DATA6
    movf    ADC_HIGH, w
    movwf   SER_DATA4
    movwf   SER_DATA5
    swapf   SER_DATA5, f
    movlw   0x0f
    andwf   SER_DATA4, f
    andwf   SER_DATA5, f
    movlw   SER_DATA4
    movwf   FSR
    goto    convertTensExact
    
    ; In this routine we convert the value in ADC_HIGH to celsius.
    ; With the op-amp in place we ignore the 2 low bits
    ; Set SER_DATA6 to zero - don't display any periods.
    ; Copy ADC_HIGH into SER_DATA4 and convert it to celsius.
    ; Calculate tens into SER_DATA5
    ; Set SER_DATA3 to 0x63 (to display as "degrees" symbol)
    ; Set SER_DATA2 to 0x0c
    ; Display the value
    ; Disale display mode decoding on digit 2 so that "degrees" is displayed
displayTemp:
    clrf    SER_DATA6
    movlw   SER_DATA4
    movwf   FSR                 ; point INDF to SER_DATA4
    movf    ADC_HIGH, w
    movwf   INDF
    bcf     STATUS, CARRYBIT    ; ensure carry bit is zero
    rrf     INDF, f             ; 1/2
    bcf     STATUS, CARRYBIT    ; ensure carry bit is zero
    rrf     INDF, f             ; 1/4
    bcf     STATUS, CARRYBIT    ; ensure carry bit is zero
    rrf     INDF, f             ; 1/8
    bcf     STATUS, CARRYBIT    ; ensure carry bit is zero
    movf    INDF, w             ; value/8 -> W
    rrf     INDF, f
    bcf     STATUS, CARRYBIT    ; ensure carry bit is zero
    addwf   INDF, w             ; add 1/16 to W
    rrf     INDF, f
    bcf     STATUS, CARRYBIT    ; ensure carry bit is zero
    addwf   INDF, w             ; add 1/32 to W
    movwf   INDF
    ; INDF (SER_DATA4) now contains celsius value
    ; subtract 10/increment SER_DATA5 until we get to zero/negative value
    bcf     STATUS, CARRYBIT    ; ensure carry bit is zero
    clrf    SER_DATA5
    movlw   0x0a
convertTensLoop:
    subwf   INDF, f
    btfss   STATUS, CARRYBIT
    goto    convertTensDone
    btfsc   STATUS, ZEROFLAG
    goto    convertTensExact
    incf    SER_DATA5, f
    goto    convertTensLoop
convertTensDone:
    ; add ten back to the units so we don't have a negative value
    addwf   INDF, f
convertTensExact:
    decf    FSR, f
    movlw   0x63
    movwf   INDF                ; 0x63 -> SER_DATA3
    decf    FSR, f
    movlw   0x0c
    // raw low value
    //movf    ADC_LOW, w
    movwf   INDF                ; 0x0c -> SER_DATA2
    decf    FSR, f
    movlw   0x20
    movwf   INDF                ; address 0x20 (Digit 0) -> SER_DATA1
    decf    FSR, f
    movlw   0x70
    movwf   INDF                ; control 0x70 -> SER_DATA0
    movlw   0x05
    movwf   BYTE_COUNT          ; set to 5 bytes
    call    displayWrite
    movlw   0x0d
    movwf   SER_DATA2
    call    setDisplayDecodeMode
    return

; ************************************************************************
; ********                  Display routines                      ********
; ************************************************************************

; Write to the display driver
displayWrite:
    bsf     APP_STATUS, 2       ; flag display write start
    call    i2cStart
    call    i2cWrite            ; control byte
    incf    FSR, f
    call    i2cWrite            ; address
dispWriteLoop:
    incf    FSR, f
    call    i2cWrite            ; data
    decfsz  BYTE_COUNT
    //goto    $-3
    goto    dispWriteLoop
    call    i2cStop
    bcf     APP_STATUS, 2       ; flag display write start
    return
    
; ************************************************************************
; ********                    RTC routines                        ********
; ************************************************************************

; Write to RTC
rtcWrite:   
    bsf     PORTC, RTC_ENABLE   ; enable RTC
    call    spiWrite
    incf    FSR, f              ; point to data
    call    spiWrite
    bcf     PORTC, RTC_ENABLE   ; disable RTC
    return

    ; read from RTC
    ;
    ; A read operation is a write of the address followed by a read/s
    ; to get the value/s.
rtcRead:    
    bsf     PORTC, RTC_ENABLE   ; enable RTC
    call    spiWrite            ; write address that we want to read
rtcReadLoop:
    incf    FSR, f
    call    spiRead
    decfsz  BYTE_COUNT, f
    //goto    $-3
    goto    rtcReadLoop
    bcf     PORTC, RTC_ENABLE   ; disable RTC
    return
  
; ************************************************************************
; ********                    SPI routines                        ********
; ************************************************************************
    
spiRead:    
    movlw   0x08                ; data bit length
    movwf   BIT_COUNT
spiReadLoop:   
    call    spiReadBit
    rlf     INDF, f
    decfsz  BIT_COUNT, f
    //goto    $-3
    goto    spiReadLoop
    bcf     PORTC, MOSI
    rlf     INDF, f
    return

spiReadBit:   
    bsf     PORTC, SPI_CLOCK
    nop
    btfsc   PORTC, MISO
    bsf     INDF, 7
    btfss   PORTC, MISO
    bcf     INDF, 7
    bcf     PORTC, SPI_CLOCK
    return

spiWrite:   
    movlw   0x08                ; data bit length
    movwf   BIT_COUNT
spiWriteLoop:   
    call    spiWriteBit
    rlf     INDF, f
    decfsz  BIT_COUNT, f
    //goto    $-3
    goto    spiWriteLoop
    rlf     INDF, f
    bcf     PORTC, MOSI
    return

; write bit 7 of INDF via SPI
spiWriteBit:  
    btfsc   INDF, 7
    bsf     PORTC, MOSI         ; set data high
    btfss   INDF, 7
    bcf     PORTC, MOSI         ; set data low
    nop
    bsf     PORTC, SPI_CLOCK
    nop
    bcf     PORTC, SPI_CLOCK
    return
    

; ************************************************************************
; ********                    I2C routines                        ********
; ************************************************************************

; I2C START bit
i2cStart:   
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_DATA     ; data  -> output
    bsf     TRISA, I2C_CLOCK    ; clock -> input
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_DATA     ; force data low
    nop
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_CLOCK    ; clock -> output
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_CLOCK    ; force clock low
    return
            
; Write a byte to I2C and wait for slave to acknowledge
i2cWrite:   
    movlw   0x08                ; data bit length
    movwf   BIT_COUNT
i2cWriteLoop:   
    btfsc   INDF, 7
    call    i2cDataHighClock
    btfss   INDF, 7
    call    i2cDataLowClock
    rlf     INDF, f
    decfsz  BIT_COUNT, f
    //goto    $-6
    goto    i2cWriteLoop
    rlf     INDF, f
    call    i2cDataRelease
    call    i2cClockRelease
    btfsc   PORTA, I2C_DATA     ; wait for ACK
    goto    $-1
    //nop                         ; TEST ONLY
    call    i2cClockLow
    return

i2cDataHighClock:   
    call    i2cDataRelease
    call    i2cClockRelease
    nop
    call    i2cClockLow
    return
            
i2cDataLowClock:   
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_DATA
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_DATA
    call    i2cClockRelease
    nop
    call    i2cClockLow
    return
            
i2cClockRelease:  
    bsf     STATUS, REGBANK     ; use bank 1
    bsf     TRISA, I2C_CLOCK
    bcf     STATUS, REGBANK     ; use bank 0
    nop
    return
            
i2cClockLow:  
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_CLOCK
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_CLOCK
    return
            
i2cDataRelease: 
    bsf     STATUS, REGBANK     ; use bank 1
    bsf     TRISA, I2C_DATA
    bcf     STATUS, REGBANK     ; use bank 0
    return
            
i2cDataLow: 
    bsf     STATUS, REGBANK     ; use bank 1
    bcf     TRISA, I2C_DATA
    bcf     STATUS, REGBANK     ; use bank 0
    bcf     PORTA, I2C_DATA
    return

i2cStop:    
    bcf     PORTA, I2C_DATA
    bsf     STATUS, REGBANK     ; use bank 1
    bsf     TRISA, I2C_CLOCK
    bsf     TRISA, I2C_DATA
    bcf     STATUS, REGBANK     ; use bank 0
    return
    
; ************************************************************************
; ********                  EEPROM routines                       ********
; ************************************************************************

; Copy BYTE_COUNT bytes from EEPROM starting at address in W register 
; into SER_DATA0 register onward. Before returning the FSR is set to
; SER_DATA0.
;
copyFromEeprom:
    bsf     STATUS, REGBANK
    movwf   EEADR
    bcf     STATUS, REGBANK
    movlw   SER_DATA0
    movwf   FSR
copyLoop:
    bsf     STATUS, REGBANK
    bsf     EECON1, 0
    movf    EEDAT, w
    incf    EEADR, f
    bcf     STATUS, REGBANK
    movwf   INDF
    incf    FSR, f
    decfsz  BYTE_COUNT, f
    goto    copyLoop
    movlw   SER_DATA0
    movwf   FSR
    return
    
; ************************************************************************
; ********              Initialization routines                   ********
; ************************************************************************

; initialise registers
initialize:       
    disableInterrupts           ; disable interrupts
    clrwdt
    bsf     STATUS, REGBANK     ; use bank 1
    movlw   0x30
    movwf   ANSEL               ; 0x30 -> ANSEL (RC1 and RC2 are analogue input)
    movlw   0x3c
    movwf   TRISA               ; 0x3c -> TRISA
    movlw   0x13
    movwf   TRISC               ; 0x13 -> TRISC
    movlw   0x3b
    movwf   OPTION_REG          ; 0x3f -> OPTION
    movlw   0x61
    movwf   OSCCON              ; 0x61 -> OSCCON
    movlw   0x30
    movwf   ADCON1              ; 0x30 -> set ADCON1 (Frc)
    movlw   0x40
    movwf   PIE1                ; enable only ADC interrupt
    bcf     STATUS, REGBANK     ; use bank 0
    clrf    CMCON0              ; comparators off
    ; Set the watchdog timer prescale rate such that wakeup is approx 8 seconds
    movlw   0x15
    movwf   WDTCON              ; 0x15 -> WDTCON (0x18)
    clrf    PORTA
    clrf    PORTC
    movlw   0x15                ; left justify, AN5 (RC1), ADC enabled
    //movlw   0x11                ; left justify, AN4 (RC0), ADC enabled
    movwf   ADCON0              ; 0x15 -> ADCON0
    call    initializeRtc
    call    initializeDisplay
    movlw   0x50
    movwf   INTCON              ; set interrupt control register
    bcf     APP_STATUS, 7       ; display time
    retfie

; initialise RTC
initializeRtc:    
    movlw   0x01
    movwf   BYTE_COUNT          ; number of bytes to read into counter
    movlw   SER_DATA1
    movwf   FSR                 ; set FSR to SER_DATA1
    movlw   0x0f
    movwf   INDF                ; set address to read Control register
    call    rtcRead
    btfss   SER_DATA2, 7        ; test !EOSC from the read register
    goto    skipRtcStartOsc     ; RTC oscillator is running
; 
; RTC oscillator is NOT running which probably indicates a fault
; Set the fail warning LED, which also prevent write to memory
; (previous code just started the oscillator)
    bcf     APP_STATUS, 0
    return
// Also should set the display to 'rtc' to indicate failure
// Also what other actions?
    
;    movlw   SER_DATA2
;    movwf   FSR
;    movlw   0x07
;    movwf   INDF               ; register value -> SER_DATA2
;    decf    FSR, f
;    movlw   0x8f
;    movwf   INDF               ; control register address -> SER_DATA1
;    call    rtcWrite           ; enable write
;    decf    FSR, f
;    call    rtcWrite           ; start oscillator and set interrupt control
;    movlw   0x00
;    movwf   INDF               ; register value -> SER_DATA2
;    decf    FSR, f
;    movlw   0x87               ; set address
;    movwf   INDF               ; alarm0 register address -> SER_DATA1
;    call    rtcWrite
skipRtcStartOsc:
    bsf     APP_STATUS, 0       ; flag RTC as good
    call    readRtcTime
    call    displayTime
    bcf     INTCON, EXT_INT
    return


; Initialise the display driver
initializeDisplay:   
    clrwdt
    movlw   0x06
    movwf   BYTE_COUNT
    movlw   0x00
    call    copyFromEeprom
    movlw   0x04
    movwf   BYTE_COUNT
    call    displayWrite
    ; set decimal point on for digit 2
    movlw   0x03
    movwf   BYTE_COUNT
    movlw   0x06
    call    copyFromEeprom
    movlw   0x01
    movwf   BYTE_COUNT
    call    displayWrite
    return
    
    end

   


